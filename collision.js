export class Collision {
    static isRecTouching(rec1x, rec1y, rec1dx, rec1dy, rec2x, rec2y, rec2dx, rec2dy) {
        return (rec1x < rec2x + rec2dx &&
            rec1x + rec1dx > rec2x &&
            rec1y < rec2y + rec2dy &&
            rec1y + rec1dy > rec2y);
    }
    static isPointInRec(pointX, pointY, recX, recY, recWidth, recHeight) {
        return (pointX >= recX &&
            pointX <= recX + recWidth &&
            pointY >= recY &&
            pointY <= recY + recHeight);
    }
    static inSceneBounds(pointX, pointY, sceneWidth, sceneHeight) {
        return pointX <= sceneWidth
            && pointX >= 0
            && pointY <= sceneHeight
            && pointY >= 0;
    }
}
