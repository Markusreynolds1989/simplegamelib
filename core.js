// GameEngine By Markus Reynolds
// GPL 3.0 License
export class Core {
    constructor(defaultBackgroundColor) {
        this.canvas = document.getElementById("canvas");
        this.ctx = this.canvas.getContext("2d");
        this.sceneWidth = this.canvas.width;
        this.sceneHeight = this.canvas.height;
        this.defaultBackgroundColor = defaultBackgroundColor;
    }
    drawImage(sprite, x, y) {
        if (this.ctx != null) {
            this.ctx.drawImage(sprite, x, y);
        }
    }
    drawText(text, x, y, size, color) {
        if (this.ctx != null) {
            this.ctx.fillStyle = color;
            this.ctx.font = `${size}pt mono`;
            this.ctx.fillText(text, x, y);
        }
    }
    drawCircle(x, y, size, color) {
        if (this.ctx != null) {
            this.ctx.beginPath();
            this.ctx.fillStyle = color;
            this.ctx.arc(x, y, size, 0, 2 * Math.PI);
            this.ctx.fill();
        }
    }
    drawCircleLines(x, y, size, color) {
        if (this.ctx != null) {
            this.ctx.beginPath();
            this.ctx.strokeStyle = color;
            this.ctx.arc(x, y, size, 0, 2 * Math.PI);
            this.ctx.lineWidth = 5;
            this.ctx.stroke();
        }
    }
    drawRectangle(x, y, width, height, color) {
        if (this.ctx != null) {
            this.ctx.fillStyle = color;
            this.ctx.fillRect(x, y, width, height);
        }
    }
    drawRectangleLines(x, y, width, height, color) {
        if (this.ctx != null) {
            this.ctx.strokeStyle = color;
            this.ctx.strokeRect(x, y, width, height);
        }
    }
    drawLine(startX, startY, endX, endY, color) {
        if (this.ctx != null) {
            this.ctx.strokeStyle = color;
            this.ctx.beginPath();
            this.ctx.moveTo(startX, startY);
            this.ctx.lineTo(endX, endY);
            this.ctx.stroke();
        }
    }
    clear() {
        if (this.ctx != null) {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.drawRectangle(0, 0, this.canvas.width, this.canvas.height, this.defaultBackgroundColor);
        }
    }
}
// Only exporting the result of dom interactions.
export let rightPressed = false, leftPressed = false, upPressed = false, downPressed = false, rightReleased = false, leftReleased = false, upReleased = false, downReleased = false, spacePressed = false, spaceReleased = false, leftClickPressed = false, leftClickReleased = false, rightClickPressed = false, rightClickReleased = false, mouseX = 0, mouseY = 0;
const keyDownHandler = (event) => {
    if (event.key == "Right" || event.key == "ArrowRight") {
        rightPressed = true;
        event.preventDefault();
    }
    else if (event.key == "Left" || event.key == "ArrowLeft") {
        leftPressed = true;
        event.preventDefault();
    }
    else if (event.key == "Up" || event.key == "ArrowUp") {
        upPressed = true;
        event.preventDefault();
    }
    else if (event.key == "Down" || event.key == "ArrowDown") {
        downPressed = true;
        event.preventDefault();
    }
    else if (event.key == " ") {
        spacePressed = true;
        event.preventDefault();
    }
};
const keyUpHandler = (event) => {
    if (event.key == "Right" || event.key == "ArrowRight") {
        rightPressed = false;
        rightReleased = true;
    }
    else if (event.key == "Left" || event.key == "ArrowLeft") {
        leftPressed = false;
        leftReleased = true;
    }
    else if (event.key == "Up" || event.key == "ArrowUp") {
        upPressed = false;
        upReleased = true;
    }
    else if (event.key == "Down" || event.key == "ArrowDown") {
        downPressed = false;
        downReleased = true;
    }
    else if (event.key == " ") {
        spacePressed = false;
        spaceReleased = true;
    }
};
const mouseMoveHandler = (event) => {
    mouseX = event.pageX;
    mouseY = event.pageY;
};
const mouseUpHandler = (event) => {
    if (event.button == 0) {
        leftClickReleased = true;
        leftClickPressed = false;
    }
    if (event.button == 2) {
        rightClickReleased = true;
        rightClickPressed = false;
    }
};
const mouseDownHandler = (event) => {
    if (event.button == 0) {
        leftClickPressed = true;
        leftClickReleased = false;
    }
    if (event.button == 2) {
        rightClickPressed = true;
        rightClickReleased = false;
    }
};
const contextHandler = (event) => event.preventDefault();
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
document.addEventListener("mousemove", mouseMoveHandler, false);
document.addEventListener("mousedown", mouseDownHandler, false);
document.addEventListener("mouseup", mouseUpHandler, false);
// So the context menu won't pop up on right click.
document.addEventListener("contextmenu", contextHandler, false);
