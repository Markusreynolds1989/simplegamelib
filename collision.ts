export class Collision {
    static isRecTouching(rec1x: number,
        rec1y: number,
        rec1dx: number,
        rec1dy: number,
        rec2x: number,
        rec2y: number,
        rec2dx: number,
        rec2dy: number): boolean {
        return (
            rec1x < rec2x + rec2dx &&
            rec1x + rec1dx > rec2x &&
            rec1y < rec2y + rec2dy &&
            rec1y + rec1dy > rec2y
        );
    }


    static isPointInRec(pointX: number,
        pointY: number,
        recX: number,
        recY: number,
        recWidth: number,
        recHeight: number): boolean {
        return (
            pointX >= recX &&
            pointX <= recX + recWidth &&
            pointY >= recY &&
            pointY <= recY + recHeight
        );
    }


    static inSceneBounds(pointX: number
        , pointY: number
        , sceneWidth: number
        , sceneHeight: number): boolean {
        return pointX <= sceneWidth
            && pointX >= 0
            && pointY <= sceneHeight
            && pointY >= 0;
    }
}
